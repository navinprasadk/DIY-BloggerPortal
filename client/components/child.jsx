import React, {Component} from 'react';
import { Input,TextArea,Form,Select,Grid,Button} from 'semantic-ui-react'

class child extends React.Component {

constructor(props){
super(props);
this.changeUnit=this.changeUnit.bind(this);
}
changeName(i,event){
  let val = event.target.value;
  console.log(val,i,'value in child');
  this.props.changeName(val,i);
}
changeQuantity(i,event){
  let val = event.target.value;
  this.props.changeQuantiy(val,i);
}
 changeUnit(i,data){
   console.log(data,"dataaaaa******");

  // console.log(`event.target.value: ${JSON.stringify(event.target.value)}`);
  //
  // console.log(`data : ${JSON.stringify(data, null, 2)}`);
  var value=data.value
	, indexOfValue = data.options.findIndex(x => x.value==value)
	, indexOfMap = data.options[indexOfValue].index;
  console.log(value, ' -- ',indexOfMap);
this.props.changeUnit(value,indexOfMap);
}
render() {
var component = this.props.MainIngred.map((item,i) =>{

  var option = [ { key: 'ounce', text: 'Ounce', value: 'Ounce',index:i  },
                  { key: 'pound', text: 'Pound', value: 'pound',index:i  },
                  { key: 'kg', text: 'Kg', value: 'kg' ,index:i },
                  { key: 'gram', text: 'gram', value: 'gram',index:i  },
                  { key: 'li', text: 'litres', value: 'li',index:i  },
				  { key: 'Number', text: 'Number', value: 'number',index:i  }
      ];
return(
<div key={i}>
<Form  >
<Grid columns={4}>
  <Grid.Row>
  <Grid.Column><input style={{backgroundColor:'#333f50'}} placeholder='Name' type="text" required="true" onBlur={this.changeName.bind(this,i)} /></Grid.Column>
  <Grid.Column><input style={{backgroundColor:'#333f50'}} placeholder='Quantity' type="number" required="true" onBlur={this.changeQuantity.bind(this,i)} /></Grid.Column>
  <Grid.Column><Select placeholder='Unit' placeholder='Unit' onChange={this.changeUnit} options={option} /></Grid.Column>
  <Grid.Column><Button style={{marginLeft:'49%',marginTop:'5%'}} onClick={this.props.addfield}>Add</Button></Grid.Column>
</Grid.Row>
</Grid>
</Form>
</div>
)
});
  return (

    <div>
    {component}
    </div>
  );

}
}
module.exports = child;
