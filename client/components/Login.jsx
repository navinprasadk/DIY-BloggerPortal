import React, {Component} from 'react';
import { Input,TextArea,Label,Icon,Segment,Form,Button,Radio, Grid} from 'semantic-ui-react';
import request from 'superagent';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';

import Cookies from 'universal-cookie';
const ReactToastr = require("react-toastr");
const {ToastContainer} = ReactToastr;
const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

const cookies = new Cookies();
//import createBrowserHistory from 'history/createBrowserHistory';
//let myApp = { history: createBrowserHistory() };
export default class Login extends React.Component {
  constructor(){
    super();
    this.state={
      username:'',
      password:'',
	  value:''
    }
    this.handleUser = this.handleUser.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.submitLogin = this.submitLogin.bind(this);
	this.handleChange =this.handleChange.bind(this);
  }
  handleUser(event){
    let val = event.target.value;
    this.setState({username:val});
  }
  handlePassword(event){
    let val = event.target.value;
    this.setState({password:val});
  }
  handleChange(e, { value }){
	  this.setState({ value:value });
  }

  // checkLoginSuccessAlert(){
  // alert("Successfully logged in");
  // }
  submitLogin(){
    
	  var Usertype = this.state.value;
    var Username = this.state.username;
    var Password = this.state.password;
    request.get('/checkVerification')
    .query({username:this.state.username})
    .set('X-API-Key', 'foobar')
    .set('Accept', 'application/json')
    .end(function(err, res){
      // this.checkLoginSuccessAlert().bind(this);
      console.log("res verification",res);
      if((err || !res.ok)){
        alert('Incorrect username or password');
      } else if(res.body.user[0].verificationStatus == true){

          request.post('/login')
          .query({username:Username,password:Password,value:Usertype})
          .set('X-API-Key', 'foobar')
          .set('Accept', 'application/json')
          .end(function(err, res){
            console.log(res);
            if ((err || !res.ok)){
              alert('Incorrect username or password');
            } else
      		  if((Usertype == res.body.value)){
              console.log(res,"response");
              console.log('user type:'+res.body.value+"state value:"+Usertype);

                cookies.set('email',res.body.email);
                hashHistory.push('/bloggerPage');
            }
      	  else
      		  alert("Incorrect user type")
          });
        }
        else{
          hashHistory.push({
            pathname: `/verify`,
            query: {email:res.body.user[0].email }
          })

        }
      });
  }
 submitRegister(){
   hashHistory.push('/register');
      //myApp.history.push('/Register');
/*    request.post('/register')
    .query({username:'lakhveer',password:'1234'})
    .end(function(err, res){
      if (err || !res.ok) {
        alert('Oh no! error');
      } else {
        alert('Logged in successfully' );
      }
    });*/

  }
render() {
    console.log('state : ',this.state);
	var abc;
	if(!cookies.get('email')){
		abc =(<div>
      <ToastContainer ref="toaster"
            toastMessageFactory = {ToastMessageFactory}
            className = "toast-top-center"/>
  <Segment>
   <h1 style={{textAlign:'left'}}>ADD TO CART LOGIN</h1>
    <h1>LOGIN</h1>
    <Form>
    <Form.Field>
      <Grid centered columns={4}><Grid.Column><label className="loginlabel">Username</label></Grid.Column>
      <Grid.Column><input style={{backgroundColor:'#333f50'}} placeholder='Username' onChange={this.handleUser}/></Grid.Column>
    </Grid>
    </Form.Field>
    <Form.Field>
      <Grid centered columns={4}><Grid.Column><label className="loginlabel">Password</label></Grid.Column>
        <Grid.Column><input style={{backgroundColor:'#333f50'}}  placeholder='Password' type="password" onChange={this.handlePassword} />  </Grid.Column>
      </Grid>
    </Form.Field>
	<Form.Field>
          <Grid centered columns={4}><Grid.Column className="loginlabel">Select User </Grid.Column>

          <Grid.Column><Radio
            label='Blogger'
            name='radioGroup'
            value='Blogger'
            checked={this.state.value === 'Blogger'}
            onChange={this.handleChange}
          />

          <Radio
            label='Retailer'
            name='radioGroup'
            value='Retailer'
            checked={this.state.value === 'Retailer'}
            onChange={this.handleChange}
          /></Grid.Column>
        </Grid>
        </Form.Field>
  <Grid centered columns={2}><Grid.Column><Button inverted color="orange" type='submit' onClick={this.submitLogin}>Login</Button>
<Button inverted color="orange" type='submit' onClick={this.submitRegister.bind(this)}>Register</Button></Grid.Column>
</Grid>

  </Form>
  </Segment>
      </div>)
	}
	else
	{
		hashHistory.push('/bloggerPage')
		location.reload();
	}
    return (
	abc
      )
    }
  }
